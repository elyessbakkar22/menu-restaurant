
###### Fonctionnalité 4 : Afficher une vue détaillée d’un plat ######

1- Description de la fonctionnalité :

* Cette fonctionnalité permet aux utilisateurs de visualiser une vue détaillée d'un plat spécifique, 
y compris son nom, son image, sa catégorie, ses ingrédients et leurs quantités.

2- Tâches à effectuer :

* Mettre en place une logique côté serveur pour récupérer les détails du plat à partir de la base de données.
* Afficher les informations du plat, y compris le nom, l'image, la catégorie, les ingrédients et les quantités.

3- Technologies et outils utilisés :

* Utilisation de Entity Framework Core pour récupérer les données du plat depuis la base de données.
* Respect de Patron de Conception (CQRS) qui sépare les deux parties de traitement (Écriture) et de réponse (Lecture).
* Héritage de notre generic repo pour tous les actions effectués sur la base de données.

4- Points d'attention :

* Assurez-vous d'optimiser les performances en minimisant les requêtes à la base de données.