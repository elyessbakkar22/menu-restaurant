﻿using System;

namespace MenuRestaurants.Common.Types
{
    public class MenuRestoException:Exception
    {
        public string Code { get; }

        public MenuRestoException()
        {
        }

        public MenuRestoException(string code)
        {
            Code = code;
        }

        public MenuRestoException(string message, params object[] args)
            : this(string.Empty, message, args)
        {
        }

        public MenuRestoException(string code, string message, params object[] args)
            : this(null, code, message, args)
        {
        }

        public MenuRestoException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public MenuRestoException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}
