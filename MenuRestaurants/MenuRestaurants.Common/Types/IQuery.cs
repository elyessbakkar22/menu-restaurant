﻿namespace MenuRestaurants.Common.Types
{
    /// <summary>
    /// Just Marker
    /// </summary>
    public interface IQuery
    {
    }

    public interface IQuery<T> : IQuery
    {
    }

}
