﻿namespace MenuRestaurants.Model.Dto
{
    public class PlatDetailsDto:PlatDto
    {
        public string Category { get; set; }
        public bool Available { get; set; }
        public IDictionary<string,string> IngredientsAndQuantites { get; set; }

    }
    public class IngredientsAndQuantitesDto
    {
        public string Ingredient { get; set; }
        public string Quantity { get; set; }
    }
}
