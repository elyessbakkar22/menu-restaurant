﻿namespace MenuRestaurants.Model.Dto
{
    public class RestaurantPlatsDto:RestaurantDto
    {
        public List<PlatDetailsDto> PlatsDetails { get; set; }
    }
}
