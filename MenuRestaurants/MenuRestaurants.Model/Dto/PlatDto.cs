﻿using MenuRestaurants.Model.Common;

namespace MenuRestaurants.Model.Dto
{
    public class PlatDto:BaseEntity
    {
        public string Name { get; set; }
        public string Image { get; set; }
    }
}
