﻿using MenuRestaurants.Model.Common;

namespace MenuRestaurants.Model.Dto
{
    public class RestaurantDto:BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
