﻿using MenuRestaurants.Model.Common;

namespace MenuRestaurants.Model.Entities
{
    public class IngredientQuantity: BaseEntity
    {
        public string Ingredient { get; set; }
        public string Quantity { get; set; }
        public int PlatId { get; set; } //forign key by convention
        public Plat Plat { get; set; } //Navigation property to Plat
    }
}
