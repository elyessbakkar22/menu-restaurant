﻿using MenuRestaurants.Model.Common;

namespace MenuRestaurants.Model.Entities
{
    public class Plat:BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public bool Available { get; set; }
        public virtual List<IngredientQuantity> IngredientsQuantities { get; set; }
        public int RestaurantId { get; set; } // Foreign key for Restaurant
        public Restaurant Restaurant { get; set; } // Navigation property to Restaurant
    }
}
