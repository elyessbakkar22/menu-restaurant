﻿using MenuRestaurants.Model.Common;

namespace MenuRestaurants.Model.Entities
{
    public class Restaurant : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public virtual List<Plat> Plats { get; set; } // Navigation property to Plat


        public void SetPlats(IEnumerable<Plat> plats)
        {
            if (plats != null)
            {
                Plats = new();
                Plats.AddRange(plats.ToList());
                SetUpdatedDate();
            }

        }
    }
}
