﻿namespace MenuRestaurants.Model.Common
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; } = DateTimeOffset.Now;
        public DateTimeOffset? LastModified { get; set; }

        protected virtual void SetUpdatedDate()
            => LastModified = DateTimeOffset.Now;
    }
}
