﻿namespace MenuRestaurants.Model.Common
{
    public class ResultDto<T>
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public T Data { get; set; }

        public ResultDto(bool isSuccess, string errorMessage, T data)
        {
            IsSuccess = isSuccess;
            ErrorMessage = errorMessage;
            Data = data;
        }
        public static ResultDto<T> Success(T data) 
            => new ResultDto<T>(true, string.Empty, data);

        public static ResultDto<T> Error(string message)
            => new ResultDto<T>(false,message,default(T));
    }
}
