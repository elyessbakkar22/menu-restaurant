﻿using MediatR;
using MenuRestaurants.Model.Common;
using System.ComponentModel.DataAnnotations;

namespace MenuRestaurants.Model.Commands
{
    public class AddPlatsRestaurant: IRequest<ResultDto<int>>
    {
        /// <summary>
        /// Restaurant Id
        /// </summary>
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The value must be greater than zero.")]
        public int RestaurantId { get; set; }
        public IEnumerable<Plat> SelectedPlats { get; set; }


    }
    public class Plat
    {
        [Required, MinLength(2)]
        public string Name { get; set; }
        [Url]
        public string Image { get; set; }
        
        [Required, MinLength(2)]
        public string Category { get; set; }
        public IEnumerable<IngredientQuantity> IngredientQuantities { get; set; }

    }
    public class IngredientQuantity
    {
        [Required, MinLength(2)]
        public string Ingredient { get; set; }
        [Required, MinLength(2)]
        public string Quantity { get; set; }
    }
}
