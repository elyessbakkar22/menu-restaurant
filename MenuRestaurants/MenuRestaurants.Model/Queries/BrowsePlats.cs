﻿using MediatR;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Model.Dto;

namespace MenuRestaurants.Model.Queries
{
    public class BrowsePlats: PagedQueryBase,IRequest<PagedResult<PlatDto>>
    {
        /// <summary>
        /// Id Restaurant
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// Name Restaurant
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// Availibility Plat
        /// </summary>
        public bool Available { get; set; } = true;
    }
}
