﻿using MediatR;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Model.Dto;

namespace MenuRestaurants.Model.Queries
{
    public class BrowsePlatsMealApi:PagedQueryBase,IRequest<PagedResult<PlatDetailsDto>>
    {
        /// <summary>
        /// Search by Name or first letter
        /// </summary>
        public string Name { get; set; }
    }
}
