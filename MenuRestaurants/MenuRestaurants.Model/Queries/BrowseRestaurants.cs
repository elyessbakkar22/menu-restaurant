﻿using MediatR;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Model.Dto;

namespace MenuRestaurants.Model.Queries
{
    public class BrowseRestaurants: PagedQueryBase, IRequest<PagedResult<RestaurantDto>>
    {

    }
}
