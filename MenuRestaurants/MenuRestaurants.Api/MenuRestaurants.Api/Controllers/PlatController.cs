﻿using MediatR;
using MenuRestaurants.Model.Dto;
using MenuRestaurants.Model.Queries;
using Microsoft.AspNetCore.Mvc;

namespace MenuRestaurants.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlatController : BaseController
    {
        private readonly ILogger<PlatController> _logger;
        public PlatController(ILogger<PlatController> logger,IMediator mediator):base(mediator)
        {
            _logger = logger;
        }
        /// <summary>
        /// fetch plats available
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("GetPlatsAvailable")]
        public async Task<IActionResult> Get([FromQuery] BrowsePlats query)
           => Collection<PlatDto>(await _mediator.Send(query));

        /// <summary>
        /// fetch plats from meals api
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("GetPlatsFromMealApi")]
        public async Task<IActionResult> GetMeals([FromQuery] BrowsePlatsMealApi query)
            => Collection<PlatDetailsDto>(await _mediator.Send(query));
    }
}
