﻿using MenuRestaurants.Model.Dto;
using MenuRestaurants.Model.Queries;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using MenuRestaurants.Model.Commands;

namespace MenuRestaurants.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantController : BaseController
    {
        private readonly ILogger<RestaurantController> _logger;
        public RestaurantController(ILogger<RestaurantController> logger, IMediator mediator):base(mediator)
        {
            _logger = logger;
        }
        /// <summary>
        /// fetch all the restaurants available
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("getRestaurants")]
        public async Task<IActionResult> Get([FromQuery] BrowseRestaurants query)
           => Collection<RestaurantDto>(await _mediator.Send(query));

        /// <summary>
        /// add some Plats selected to a restaurant
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("addPlatsRestaurant")]
        public async Task<IActionResult> Post([FromBody] AddPlatsRestaurant command)
        { 
            var result = await _mediator.Send(command);
            return result.IsSuccess ? Ok(result) : BadRequest(result.ErrorMessage);
        }


    }
}
