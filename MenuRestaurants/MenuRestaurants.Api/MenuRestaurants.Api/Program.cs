using MenuRestaurants.Data.Context;
using Microsoft.EntityFrameworkCore;
using Autofac;
using MenuRestaurants.Data.Repositories;
using Autofac.Extensions.DependencyInjection;
using System.Reflection;
using MenuRestaurants.Api.ApiServices.MealApi;
using MenuRestaurants.Common.RestEase;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.
        AddDbContext<DatabaseContext>(x => {

            bool useInMemoryProvider = bool.Parse(builder.Configuration["AppSettings:InMemoryProvider"]);

            switch (useInMemoryProvider)
            {
                case true:
                    x.UseInMemoryDatabase("menuRestoDB");
                    break;
                case false:
                    x.UseSqlServer(builder.Configuration.GetConnectionString("MenuRestoConnection"));
                    break;
            }
        });
builder.Services.RegisterServiceForwarder<IMealApiService>(builder.Configuration,"mealDb-service");

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

// i used here autofac to achieve similar registration of repository classes.
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());

builder.Host.ConfigureContainer<ContainerBuilder>(builder =>
{
    builder.RegisterAssemblyTypes(typeof(PlatRepository).Assembly)
                   .Where(classe => classe.Name.EndsWith("Repository"))
                   .AsImplementedInterfaces().InstancePerLifetimeScope();
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

//DatabaseInitialiser.Initialize(app.Services);

app.Run();
