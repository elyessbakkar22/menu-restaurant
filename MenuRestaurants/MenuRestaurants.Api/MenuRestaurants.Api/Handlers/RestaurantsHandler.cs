﻿using MenuRestaurants.Model.Dto;
using MenuRestaurants.Model.Queries;
using MenuRestaurants.Common.Dispatchers;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Data.Repositories;
using MediatR;

namespace MenuRestaurants.Api.Handlers
{
    public class RestaurantsHandler : IRequestHandler<BrowseRestaurants, PagedResult<RestaurantDto>>
    {
        private readonly IRestaurantRepository _restaurantRepository;
        public RestaurantsHandler(IRestaurantRepository restaurantRepository)
        {
            _restaurantRepository = restaurantRepository;
        }

        public async Task<PagedResult<RestaurantDto>> Handle(BrowseRestaurants request, CancellationToken cancellationToken)
        {
            var pagedResult = await _restaurantRepository.BrowseAsync(request);
            var restaurants = pagedResult.Items.Select(p => new RestaurantDto
            {
                Id = p.Id,
                Name = p.Name,
            }).ToList();
            return PagedResult<RestaurantDto>.From(pagedResult, restaurants);
        }
    }
}
