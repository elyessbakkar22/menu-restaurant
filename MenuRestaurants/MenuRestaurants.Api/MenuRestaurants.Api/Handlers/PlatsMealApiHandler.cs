﻿using MediatR;
using MenuRestaurants.Api.ApiServices.MealApi;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Data.Common;
using MenuRestaurants.Model.Dto;
using MenuRestaurants.Model.Queries;
using Newtonsoft.Json;

namespace MenuRestaurants.Api.Handlers
{
    public class PlatsMealApiHandler : IRequestHandler<BrowsePlatsMealApi, PagedResult<PlatDetailsDto>>
    {
        private readonly IMealApiService _mealApiService;
        public PlatsMealApiHandler(IMealApiService mealApiService)
        {
            _mealApiService = mealApiService;
        }
        public async Task<PagedResult<PlatDetailsDto>> Handle(BrowsePlatsMealApi request, CancellationToken cancellationToken)
        {
            var platsMeals = await _mealApiService.BrowseAsync(request.Name);
            //map response with our Dto
            var pagedResult = platsMeals.meals.Select(meal =>
            {
                var ingredientQuantityPairs = new Dictionary<string, string>();
                for (int i = 1; i <= 20; i++)
                {
                    // Retrieve ingredient and measure values
                    string ingredient = (string)meal.GetType().GetProperty($"strIngredient{i}").GetValue(meal);
                    string quantity = (string)meal.GetType().GetProperty($"strMeasure{i}").GetValue(meal);
                    if (!string.IsNullOrEmpty(ingredient) 
                                && !string.IsNullOrEmpty(quantity) 
                                        && !ingredientQuantityPairs.ContainsKey(ingredient))
                    {
                        ingredientQuantityPairs.Add(ingredient, quantity);
                        //ingredientMeasurePairs.Add($"Quantity{i}", quantity);
                    }
                }
              
                return new PlatDetailsDto()
                {
                    Available = true,
                    Category = meal.strCategory,
                    Name = meal.strMeal,
                    Image = meal.strMealThumb,
                    IngredientsAndQuantites = ingredientQuantityPairs
                    //.Select(x => new IngredientsAndQuantitesDto() { Ingredient = x.Key,Quantity = x.Value})
                };
            }).Paginate();
            
            return PagedResult<PlatDetailsDto>.From(pagedResult,pagedResult.Items);
        }
    }
}
