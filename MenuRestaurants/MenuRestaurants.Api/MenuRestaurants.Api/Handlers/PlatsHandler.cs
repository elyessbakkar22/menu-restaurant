﻿using MediatR;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Data.Repositories;
using MenuRestaurants.Model.Dto;
using MenuRestaurants.Model.Queries;

namespace MenuRestaurants.Api.Handlers
{
    public class PlatsHandler : IRequestHandler<BrowsePlats, PagedResult<PlatDto>>
    {
        private readonly IPlatRepository _platRepository;
        public PlatsHandler(IPlatRepository platRepository)
        {
            _platRepository = platRepository;
        }

        public async Task<PagedResult<PlatDto>> Handle(BrowsePlats request, CancellationToken cancellationToken)
        {
            var pagedResult = await _platRepository.BrowseAsync(x => x.Available == request.Available 
                                                                && (request.Name == null || x.Restaurant.Name.Contains(request.Name))
                                                                && (request.Id == null || x.RestaurantId == request.Id)
                                                                ,request);
            var plats = pagedResult.Items.Select(p => new PlatDto
            {
                Id = p.Id,
                Name = p.Name,
            }).ToList();
            return PagedResult<PlatDto>.From(pagedResult, plats);
        }
    }
}
