﻿using MediatR;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Data.Repositories;
using MenuRestaurants.Model.Commands;
using MenuRestaurants.Model.Common;
using MenuRestaurants.Model.Dto;
using MenuRestaurants.Model.Entities;
using IngredientQuantity = MenuRestaurants.Model.Entities.IngredientQuantity;
using Plat = MenuRestaurants.Model.Entities.Plat;

namespace MenuRestaurants.Api.Handlers
{
    public class AddPlatsRestaurantHandler : IRequestHandler<AddPlatsRestaurant, ResultDto<int>>
    {
        private readonly IRestaurantRepository _restaurantRepository;
        public AddPlatsRestaurantHandler(IRestaurantRepository restaurantRepository)
        {
            _restaurantRepository = restaurantRepository;
        }
        public async Task<ResultDto<int>> Handle(AddPlatsRestaurant request, CancellationToken cancellationToken)
        {
            var restaurant = await _restaurantRepository.GetAsync(request.RestaurantId);

            if (restaurant == null)
                throw new MenuRestoException("restaurant_not_found",
                    $"Restaurant with id: '{request.RestaurantId}' was not found.");

            var selectedPlats = request.SelectedPlats.Select(x => new Plat()
            {
                Name = x.Name,
                Image = x.Image,
                Category = x.Category,
                Available = true,
                IngredientsQuantities = x.IngredientQuantities
                                            .Select(i => new IngredientQuantity()
                                            {
                                                Quantity = i.Quantity,
                                                Ingredient = i.Ingredient
                                            }).ToList()
            });
            restaurant.SetPlats(selectedPlats);
     
            await _restaurantRepository.UpdateAsync(restaurant, request.RestaurantId);
            return ResultDto<int>.Success(restaurant.Id);
        }
    }
}
