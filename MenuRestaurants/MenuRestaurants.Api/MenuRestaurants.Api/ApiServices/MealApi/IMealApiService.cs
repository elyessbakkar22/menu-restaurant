﻿using RestEase;

namespace MenuRestaurants.Api.ApiServices.MealApi
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
    public interface IMealApiService
    {
        [AllowAnyStatusCode]
        [Get("1/search.php?s={query}")]
        Task<MealsDbDto> BrowseAsync([Path] string query);
    }
}
