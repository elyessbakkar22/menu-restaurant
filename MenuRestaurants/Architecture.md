###### Architecture ######

1- Schéma de base de données:

* nous allons opter pour une approche relationnelle la nature structurée des données de l'application,
par la suite nous allons utilisé une base de données relationnelle pour stocker les données.

* les principales entités et leurs relations:
  
  - Restaurant : Contient des détails sur les restaurants, tels que leur nom, leur adresse, etc.
  
  - Plat : Contient des détails sur les plats proposés par les restaurants, 
		   avec une relation many-to-one avec les restaurants.
  
  - IngredientQuantity:Contient des les ingredients et les quantités dans un plat,
					   avec une realation many-to-one avec les plats.


2- les différentes couches de l'application:
  
  2-1 Couche Présentation (MenuRestaurants.Api):
	
	* Cette couche gère l'interface utilisateur de l'application.
	* Elle comprend les contrôleurs,les gestionnaires logique (handlers),
	  les interfaces services RESTful comme MealDbApi.

  2-2 Couche Services (Projeté mais non réalisé):
     
	 * Pour le moment,notre application est relativement simple et ne comporte pas de logique métier complexe, 
	   il peut être judicieux de ne pas introduire de couche de services supplémentaire,
	   en utilisant directement les gestionnaires (handlers) pour répondre aux demandes
  
  2-3 Couche Accès aux données (MenuRestaurants.Data):
	
	* Cette couche est responsable de l'accès aux données de l'application.
	* Elle interagit directement avec la base de données pour effectuer des opérations CRUD (Create, Read, Update, Delete).
  
  2-4 Couche Model (MenuRestaurants.Model):
    
	* Elle comprend les classes de contexte de base de données, les modèles demande CQRS (Query,Command), 
	  les models de réponse (Dto).

  2-5 Couche Commune (MenuRestaurants.Common):
	
	* regrouper des fonctionnalités communes dans une couche séparée, 
	  on peut les réutiliser facilement dans différentes parties de l'application sans avoir à dupliquer le code.


3- Schéma de flux entre les couche:

   3-1 Fonctionnalité:Affichage de la liste des restaurants

     API → Handler de Gestion des Restaurants :
		* La couche API reçoit une requête HTTP GET pour récupérer la liste des restaurants.
		* La couche API transmet la demande au Service de Gestion des Restaurants pour le traitement métier.
     handler de Gestion des Restaurants → Couche Accès aux Données :
	    * Le handler de Gestion des Restaurants transmet la demande à la Couche Accès aux Données pour accéder aux données des restaurants.
	 Accès aux Données → Base de Données :
	    * La Couche Accès aux Données effectue une requête vers la base de données pour récupérer les données des restaurants.
		* La Base de Données renvoie les données demandées à la Couche Accès aux Données.
	 Accès aux Données → le handler de Gestion des Restaurants :
        * La Couche Accès aux Données transmet les données récupérées au handler de Gestion des Restaurants pour traitement.
	 Handler de Gestion des Restaurants → API :
	    * Le handler renvoie les données des restaurants à la couche API.


   3-2 Fonctionnalité:Afficher les plats disponibles dans un restaurant

     API → Handler de Gestion des Plats :
		* La couche API reçoit une requête HTTP GET pour afficher les plats disponibles dans un restaurant.
		* La demande est transmise directement au Service de Gestion des Plats pour le traitement métier.
     handler de Gestion des Plats → Couche Accès aux Données :
	    * Le handler de Gestion des Plats transmet la demande à la Couche Accès aux Données pour accéder aux données des plats du restaurant.
	 Accès aux Données → Base de Données :
	    * La Couche Accès aux Données effectue une requête vers la base de données pour récupérer les données des plats.
		* La Base de Données renvoie les données demandées à la Couche Accès aux Données.
	 Accès aux Données → le handler de Gestion des Plats :
        * La Couche Accès aux Données transmet les données récupérées au handler de Gestion des Plats pour traitement.
	 Handler de Gestion des Plats → API :
	    * Le handler renvoie les données des plats disponible à la couche API.

   
   3-3 Fonctionnalité:Pouvoir ajouter de nouveaux plats dans le menu d’un restaurant grâce à des propositions de
                      l’API TheMealDb.

     API → Handler de Gestion des Restaurants :
		* La couche API reçoit une requête HTTP POST pour ajouter de nouveaux plats dans le menu d'un restaurant, 
		  avec des propositions plats a selectionnés, fournit par une requête HTTP GET (GetPlatsFromMealApi) a la responsabilité de communiquer avec l'API TheMealDb.
		* La couche API transmet la demande au Service de Gestion des Restaurants pour le traitement métier.
     handler de Gestion des Restaurants → Couche Accès aux Données :
	    * Le handler de Gestion des Restaurants transmet la demande à la Couche Accès aux Données pour effectuer l'ajout de nouveaux plats dans la base de données.
	 Accès aux Données → Base de Données :
	    * La Couche Accès aux Données effectue une requête vers la base de données pour ajouter les nouveaux plats dans le menu du restaurant.
	 Accès aux Données → le handler de Gestion des Restaurants :
        * Aprés la confirmation du réussite de l'opération au niveau base de données,la Couche Accès aux Données transmet la confirmation de l'ajout des nouveaux plats au handler de Gestion des Restaurants.
	 Handler de Gestion des Restaurants → API :
	    * Le Handler renvoie une réponse HTTP contenant le résultat de l'opération d'ajout de nouveaux plats dans le menu du restaurant à la couche API
	 


4- Choix effectués et risques liés à ce type d'architecture et les Possibilités d'évolution:

  4-1 Choix effectués :

	* Utilisation d'une architecture en couches pour une séparation claire des responsabilités.
	* Utilisation de SQL Server comme base de données relationnelle en raison de sa robustesse et de sa fiabilité.

  4-2 Risques liés :

    * Complexité accrue due à la gestion de plusieurs couches et de leurs interactions.
	* Risque de performance si les interactions entre les couches ne sont pas optimisées.
  
  4-3 Possibilités d'évolution:
    
	* Ajout de nouvelles fonctionnalités sans perturber les couches existantes grâce à la séparation.
	* Passage à une architecture microservices si l'application devient complexe et nécessite une évolutivité horizontale.
    



