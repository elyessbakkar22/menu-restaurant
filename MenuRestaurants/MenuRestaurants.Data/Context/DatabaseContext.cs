﻿using MenuRestaurants.Model.Entities;
using Microsoft.EntityFrameworkCore;

namespace MenuRestaurants.Data.Context
{
    public class DatabaseContext: DbContext
    {
        public DatabaseContext(DbContextOptions dbContextOptions):base(dbContextOptions)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Plat> Plats { get; set; }
        public DbSet<IngredientQuantity> IngredientsQuantities { get; set;}
    }
}
