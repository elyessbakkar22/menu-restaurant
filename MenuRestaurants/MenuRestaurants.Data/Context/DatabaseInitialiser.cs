﻿using MenuRestaurants.Model.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace MenuRestaurants.Data.Context
{
    public class DatabaseInitialiser
    {
        private static DatabaseContext context;
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var serviceScope = serviceProvider.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DatabaseContext>();
                SeedRestaurants(context);
            }
        }

        private static void SeedRestaurants(DatabaseContext? context)
        {
            var restaurants = new List<Restaurant>()
            {
                new()
                {
                    Name = "Restaurant Bon - Paris 16",
                    Address ="25 Rue de la Pompe",
                    Plats = new List<Plat>()
                    {
                       new(){  Name = "Magret de canard",Category = "Plat",Available = true},
                       new(){  Name = "Moules-frites",Category = "Plat2",Available = true}

                    }
                },
                new()
                {
                    Name = "To Restaurant Paris",
                    Address ="34 Rue Beaurepaire",
                    Plats = new List<Plat>()
                    {
                       new(){  Name = "Blanquette de veau",Category = "Plat3",Available = true},
                       new(){  Name = "Côte de bœuf",Category = "Plat4",Available = true}

                    }
                },
                new()
                {
                    Name = "Restaurant H",
                    Address ="13 Rue Jean Beausire",
                    Plats = new List<Plat>()
                    {
                       new(){  Name = "Gigot d'agneau",Category = "Plat5",Available = true},
                       new(){  Name = "Steak-frites",Category = "Plat6",Available = true}
                    }
                    
                }

            };
            context.Restaurants.AddRange(restaurants);
            context.SaveChanges();
        }
    }
}
