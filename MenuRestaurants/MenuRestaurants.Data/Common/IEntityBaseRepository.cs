﻿using MenuRestaurants.Common.Types;
using System.Linq.Expressions;

namespace MenuRestaurants.Data.Common
{
    public interface IEntityBaseRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetAsync(object id);
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, string includeProperties = "");
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<PagedResult<TEntity>> BrowseAsync<TQuery>(TQuery query, params Expression<Func<TEntity, object>>[] includes) where TQuery : PagedQueryBase;
        Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate,
                TQuery query) where TQuery : PagedQueryBase;

        Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate, TQuery query, params Expression<Func<TEntity, object>>[] includes) where TQuery : PagedQueryBase;
        Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate, TQuery query, string includeProperties = "") where TQuery : PagedQueryBase;
        Task<TEntity> AddItemAsync(TEntity entity);
        Task UpdateAsync(TEntity entity, object id);
        Task<TEntity> DeleteAsync(object id);
        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate);
        Task<List<TEntity>> AddRangeItemsAsync(List<TEntity> entities);
        Task<IEnumerable<TEntity>> DeleteRangeAsync(Expression<Func<TEntity, bool>> predicate);
    }

}
