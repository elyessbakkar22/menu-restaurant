﻿using MenuRestaurants.Common.Types;
using Microsoft.EntityFrameworkCore;

namespace MenuRestaurants.Data.Common
{
    public static class PaginationExtensions
    {
        public static async Task<PagedResult<T>> PaginateAsync<T>(this IQueryable<T> collection, PagedQueryBase query)
            => await collection.PaginateAsync(query.Page, query.Results);

        public static async Task<PagedResult<T>> PaginateAsync<T>(this IQueryable<T> collection,
            int page = 1, int resultsPerPage = 10)
        {
            if (page <= 0)
            {
                page = 1;
            }
            if (resultsPerPage <= 0)
            {
                resultsPerPage = 10;
            }

            var totalResults = await collection.CountAsync();
            var data = await collection.Skip((page - 1) * resultsPerPage).Take(resultsPerPage).ToListAsync();
            var totalPages = (int)Math.Ceiling((decimal)totalResults / resultsPerPage);

            return PagedResult<T>.Create(data, page, resultsPerPage, totalPages, totalResults);
        }
        public static PagedResult<T> Paginate<T>(this IEnumerable<T> collection,
            int page = 1, int resultsPerPage = 10)
        {
            if (page <= 0)
            {
                page = 1;
            }
            if (resultsPerPage <= 0)
            {
                resultsPerPage = 10;
            }

            var totalResults = collection.Count();
            var data = collection.Skip((page - 1) * resultsPerPage).Take(resultsPerPage).ToList();
            var totalPages = (int)Math.Ceiling((decimal)totalResults / resultsPerPage);

            return PagedResult<T>.Create(data, page, resultsPerPage, totalPages, totalResults);
        }
    }
}
