﻿using MenuRestaurants.Common.Types;
using MenuRestaurants.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace MenuRestaurants.Data.Common
{
    public class EntityBaseRepository<TEntity> : IEntityBaseRepository<TEntity>
        where TEntity : class
    {

        protected DatabaseContext _context;
        private DbSet<TEntity> _objectSet;

        #region Properties
        public EntityBaseRepository(DatabaseContext context)
        {
            _context = context;
            _objectSet = context.Set<TEntity>();
        }
        #endregion
        public async Task<TEntity> GetAsync(object id)
             => await _objectSet.FindAsync(id);

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate)
            => await _objectSet.Where(predicate).SingleOrDefaultAsync();

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            var dynamicQuery = _objectSet.AsQueryable();
            if (includes != null)
            {
                dynamicQuery = includes.Aggregate(dynamicQuery,
                          (current, include) => current.Include(include));
            }

            return await dynamicQuery.FirstOrDefaultAsync(predicate);
        }
        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, string includeProperties = "")
        {
            var dynamicQuery = _objectSet.Where(predicate).AsQueryable();
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                dynamicQuery = dynamicQuery.Include(includeProperty).AsNoTracking();
            }

            return await dynamicQuery.AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
            => await _objectSet.Where(predicate).ToListAsync();

        public async Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate,
                TQuery query) where TQuery : PagedQueryBase
        {
            var dynamicQuery = _objectSet.Where(predicate).AsQueryable();
            if (query.OrderBy != null)
                dynamicQuery = dynamicQuery.CustomOrderBy(query);

            return await dynamicQuery.PaginateAsync(query);
        }
        public async Task<PagedResult<TEntity>> BrowseAsync<TQuery>(TQuery query, params Expression<Func<TEntity, object>>[] includes) where TQuery : PagedQueryBase
        {
            var dynamicQuery = _objectSet.AsQueryable();
            if (includes != null)
            {
                dynamicQuery = includes.Aggregate(dynamicQuery,
                          (current, include) => current.Include(include));
            }
            if (query.OrderBy != null)
                dynamicQuery = dynamicQuery.CustomOrderBy(query);

            return await dynamicQuery.PaginateAsync(query);
        }
        public async Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate, TQuery query, params Expression<Func<TEntity, object>>[] includes) where TQuery : PagedQueryBase
        {
            var dynamicQuery = _objectSet.AsQueryable().Where(predicate);
            if (includes != null)
            {
                dynamicQuery = includes.Aggregate(dynamicQuery,
                          (current, include) => current.Include(include));
            }
            if (query.OrderBy != null)
                dynamicQuery = dynamicQuery.CustomOrderBy(query);


            return await dynamicQuery.PaginateAsync(query);
        }
        public async Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate, TQuery query, string includeProperties = "") where TQuery : PagedQueryBase
        {
            var dynamicQuery = _objectSet.AsQueryable().Where(predicate).AsNoTracking();
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                dynamicQuery = dynamicQuery.Include(includeProperty).AsNoTracking();
            }

            if (query.OrderBy != null)
                dynamicQuery = dynamicQuery.CustomOrderBy(query);

            return await dynamicQuery.AsNoTracking().PaginateAsync(query);
        }

        public async Task<TEntity> AddItemAsync(TEntity entity)
        {
            await _objectSet.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }
        public async Task<List<TEntity>> AddRangeItemsAsync(List<TEntity> entities)
        {
            await _objectSet.AddRangeAsync(entities);
            await _context.SaveChangesAsync();
            return entities;
        }
        public async Task UpdateAsync(TEntity entity, object id)
        {
            TEntity exist = await _objectSet.FindAsync(id);
            if (exist != null)
            {
                _context.Entry(exist).CurrentValues.SetValues(entity);
                
            }
             await _context.SaveChangesAsync();
        }
        public async Task<TEntity> DeleteAsync(object id)
        {
            var entity = await _objectSet.FindAsync(id);
            if (entity != null)
            {
                _objectSet.Remove(entity);
                await _context.SaveChangesAsync();
            }
            return entity;
        }
        public async Task<IEnumerable<TEntity>> DeleteRangeAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var entities = await _objectSet.Where(predicate).ToListAsync();
            if (entities != null)
            {
                _objectSet.RemoveRange(entities);
                await _context.SaveChangesAsync();
            }
            return entities;
        }
        public async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate)
            => await _objectSet.AnyAsync(predicate);

    }
}
