﻿using MenuRestaurants.Common.Types;
using System.Reflection;

namespace MenuRestaurants.Data.Common
{
    public static class CustomOrderExtensions
    {
        public static IOrderedQueryable<TSource> CustomOrderBy<TSource>(this IQueryable<TSource> source, PagedQueryBase query)
        {
            var orderByField = query.OrderBy.ToLower();
            Func<TSource, object> func = (TSource a) => { return GetPropValue(a, orderByField); };
            switch (query.SortOrder)
            {
                case "DESC":
                case "desc":
                    return source.OrderByDescending(p => func(p));
                case "ASC":
                case "asc":
                    return source.OrderBy(p => func(p));
                default: return (IOrderedQueryable<TSource>)source;
            }
        }
        
        public static object? GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName, BindingFlags.IgnoreCase |
                BindingFlags.Public | BindingFlags.Instance)?.GetValue(src, null);
        }

    }
}
