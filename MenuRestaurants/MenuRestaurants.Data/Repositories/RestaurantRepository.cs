﻿using MenuRestaurants.Model.Queries;
using MenuRestaurants.Common.Types;
using MenuRestaurants.Data.Common;
using MenuRestaurants.Data.Context;
using MenuRestaurants.Model.Entities;
using System.Linq.Expressions;

namespace MenuRestaurants.Data.Repositories
{
    public class RestaurantRepository : EntityBaseRepository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<PagedResult<Restaurant>> BrowseAsync(BrowseRestaurants query)
        {
           return await base.BrowseAsync(query);
        }

        public Task<bool> ExistsAsync(int id)
        {
            return base.ExistsAsync(x => x.Id == id);
        }
        public async Task<Restaurant> GetAsync(int id)
        {
            return await base.GetAsync(id);
        }
        public async Task<Restaurant> GetIncludesAsync(Expression<Func<Restaurant, bool>> predicate, params Expression<Func<Restaurant, object>>[] includes)
        {
            return await base.GetIncludesAsync(predicate,includes);
        }
    }
    public interface IRestaurantRepository : IEntityBaseRepository<Restaurant>
    {
        Task<Restaurant> GetAsync(int id);
        Task<Restaurant> GetIncludesAsync(Expression<Func<Restaurant, bool>> predicate, params Expression<Func<Restaurant, object>>[] includes);
        Task<bool> ExistsAsync(int id);
        Task<PagedResult<Restaurant>> BrowseAsync(BrowseRestaurants query);

    }
}
