﻿using MenuRestaurants.Common.Types;
using MenuRestaurants.Data.Common;
using MenuRestaurants.Data.Context;
using MenuRestaurants.Model.Entities;
using MenuRestaurants.Model.Queries;
using System.Linq.Expressions;

namespace MenuRestaurants.Data.Repositories
{
    public class PlatRepository : EntityBaseRepository<Plat>, IPlatRepository
    {
        public PlatRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<PagedResult<Plat>> BrowseAsync(BrowsePlats query)
        {
            return await base.BrowseAsync(query);
        }
        public async Task<PagedResult<Plat>> BrowseAsync<TQuery>(Expression<Func<Plat, bool>> predicate, TQuery query, params Expression<Func<Plat, object>>[] includes) where TQuery : PagedQueryBase
        {
            return await base.BrowseAsync(predicate, query, includes);
        }
       
    }
    public interface IPlatRepository : IEntityBaseRepository<Plat>
    {
        Task<PagedResult<Plat>> BrowseAsync(BrowsePlats query);
        Task<PagedResult<Plat>> BrowseAsync<TQuery>(Expression<Func<Plat, bool>> predicate, TQuery query, params Expression<Func<Plat, object>>[] includes) where TQuery : PagedQueryBase;
    }
}
