###### Fonctionnalité 2 : Ajouter de nouveaux restaurants ######

1- Description de la fonctionnalité :

* Cette fonctionnalité permet aux utilisateurs d'ajouter de nouveaux restaurants à la base de données de l'application.
* Les utilisateurs doivent fournir les informations suivantes : nom du restaurant,adresse etc.

2- Tâches à effectuer :

* Créer une logique côté serveur pour valider les données entrées par l'utilisateur.
* Implémenter une méthode dans le handler ou la couche service ou repos pour ajouter un nouveau restaurant à la base de données.
* Gérer les erreurs et les exceptions qui pourraient survenir lors de l'ajout du restaurant.

3- Technologies et outils utilisés :

* Utilisation de Entity Framework Core et le couche (generic repository) pour interagir avec la base de données.
* Respect de Patron de Conception (CQRS) qui sépare les deux parties de traitement (Écriture) et de réponse (Lecture).

4- Points d'attention :

* Assurez-vous d'optimiser les performances en minimisant les requêtes à la base de données.